import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from '@/filters/currency.filter'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'
import messagePlugin from '@/utils/message.plugin'
import Loader from '@/components/app/Loader'


import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.component('Loader', Loader)

firebase.initializeApp({
  apiKey: "AIzaSyDFDOavFBfvJhq7e1fpaj2ly6lS5VLxURw",
  authDomain: "budget-12efd.firebaseapp.com",
  databaseURL: "https://budget-12efd.firebaseio.com",
  projectId: "budget-12efd",
  storageBucket: "budget-12efd.appspot.com",
  messagingSenderId: "212607739110",
  appId: "1:212607739110:web:409886b1383cf78ee6c727",
  measurementId: "G-BYCDWJ539D"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
